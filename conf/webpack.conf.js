const webpack = require('webpack');
const conf = require('./gulp.conf');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const yargs = require('yargs').argv;

const environment = yargs.env || 'dev';

const envConfiguration = {
    dev: {
      apiKey: "AIzaSyAHIy9YSETftq54BuKY0_3Cr8f7-8wbJz8",
      authDomain: "fifco-8eb45.firebaseapp.com",
      databaseURL: "https://fifco-8eb45.firebaseio.com",
      storageBucket: "fifco-8eb45.appspot.com",
      messagingSenderId: "579262109618"
    },
    staging: {
      apiKey: "AIzaSyAHIy9YSETftq54BuKY0_3Cr8f7-8wbJz8",
      authDomain: "fifco-8eb45.firebaseapp.com",
      databaseURL: "https://fifco-8eb45.firebaseio.com",
      storageBucket: "fifco-8eb45.appspot.com",
      messagingSenderId: "579262109618"
    },
    prod: {
      apiKey: "AIzaSyAHIy9YSETftq54BuKY0_3Cr8f7-8wbJz8",
      authDomain: "fifco-8eb45.firebaseapp.com",
      databaseURL: "https://fifco-8eb45.firebaseio.com",
      storageBucket: "fifco-8eb45.appspot.com",
      messagingSenderId: "579262109618"
    }
}[environment];

module.exports = {
  module: {
    loaders: [
      {
        test: /.json$/,
        loaders: [
          'json'
        ]
      },
      {
        test: /\.(css|less)$/,
        loader: "style-loader!css-loader!less-loader!postcss-loader"
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: [
          'ng-annotate',
          'babel'
        ]
      },
      {
        test: /.html$/,
        loaders: [
          'html'
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|svg|png|gif)$/,
        loader: 'url'
      }
    ]
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NoErrorsPlugin(),
    new HtmlWebpackPlugin({
      template: conf.path.src('index.html'),
      inject: true
    }),
    new webpack.DefinePlugin({FIREBASE_CONFIG: JSON.stringify(envConfiguration)})
  ],
  postcss: () => [autoprefixer({ browsers: ['last 2 versions'] })],
  debug: true,
  devtool: 'cheap-module-eval-source-map',
  output: {
    path: path.join(process.cwd(), conf.paths.tmp),
    filename: 'index.js'
  },
  entry: `./${conf.path.src('index')}`,
  node: {
    fs: "empty"
  }
};

