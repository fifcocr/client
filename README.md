# Fifco

Administrative app for fifco.

## Set Up
NPM have to be installed to set up the project.

Run npm to get all dependencies needed.
```sh
  npm install
```

## Build & development
For running the app use `npm run serve` 

----------
Crafted by:
[![Pernix-Solutions.com](http://pernix.cr/static/images/pernix-logo.svg)
](http://Pernix-Solutions.com)
