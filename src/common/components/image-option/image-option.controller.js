class ImageOptionController {

  /* @ngInject */
  constructor($scope) {
    const vm = this;
    vm.toggleSelected = toggleSelected;

    if (vm.model === vm.text) {
      vm.selected = true;
    }

    $scope.$watch('vm.model', newValue => {
      if (newValue !== vm.text) {
        vm.selected = false;
      }
    });

    function toggleSelected() {
      vm.selected = !vm.selected;
      vm.model = vm.selected ? vm.text : '';
    }
  }
}

export default ImageOptionController;

