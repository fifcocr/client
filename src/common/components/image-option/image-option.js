import ImageOptionController from './image-option.controller';
import './image-option.less';

const imageOption = {
  template: require('./image-option.html'),
  controller: ImageOptionController,
  controllerAs: 'vm',
  bindings: {
    src: '@',
    text: '@',
    name: '@',
    model: '='
  }
};

export default
  angular
    .module('image-option', [])
    .component('imageOption', imageOption)
    .name;

