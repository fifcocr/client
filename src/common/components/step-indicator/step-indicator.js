import 'angular-wizard';
import StepIndicatorController from './step-indicator.controller';
import './step-indicator.less';

const stepIndicator = {
  template: require('./step-indicator.html'),
  controller: StepIndicatorController,
  controllerAs: 'vm'
};

export default
  angular
    .module('step-indicator', [
      'mgo-angular-wizard'
    ])
    .component('stepIndicator', stepIndicator)
    .name;

