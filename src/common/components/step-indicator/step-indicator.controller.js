class StepIndicatorController {

  /* @ngInject */
  constructor($state, WizardHandler) {
    const vm = this;
    const stateName = 'newTrip.step';
    vm.next = next;
    vm.counter = 1;
    vm.state = $state.current.name;

    function next() {
      if (vm.counter < 5) {
        vm.counter += 1;
        $state.go(stateName + vm.counter);
        WizardHandler.wizard().next();
      }
    }
  }
}

export default StepIndicatorController;

