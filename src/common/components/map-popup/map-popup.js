const mapPopup = {
  template: require('./map-popup.html'),
  controllerAs: 'vm',
  bindings: {
    trip: '='
  }
};

export default
  angular
    .module('map-popup', [])
    .component('mapPopup', mapPopup)
    .name;
