import LeafletController from './leaflet.controller';
import mapPopup from '../map-popup/map-popup';
import leafletService from './leaflet.service';
import './leaflet.less';

const leaflet = {
  template: require('./leaflet.html'),
  controller: LeafletController,
  controllerAs: 'vm',
  bindings: {
    name: '@',
    width: '@',
    height: '@',
    latitude: '@',
    longitude: '@',
    actualZoom: '@',
    minZoom: '@',
    maxZoom: '@',
    markerList: '=',
    origin: '=',
    destination: '='
  }
};

export default
  angular
    .module('Leaflet', [
      mapPopup
    ])
    .component('leaflet', leaflet)
    .factory('leafletService', leafletService)
    .name;
