function leafletService() {
  return {
    drawMap(mapName, center, zoom) {
      const map = L.map(mapName, {
        zoomControl: false,
        center: L.latLng(center.latitude, center.longitude),
        zoom: zoom.actual,
        minZoom: zoom.minZoom,
        maxZoom: zoom.maxZoom
      });
      return map;
    },
    addLayer(map) {
      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'})
      .addTo(map);
    },
    addZoomControl(map) {
      L.control.zoom().addTo(map);
    },
    addSearchControl(description, map, callback) {
      L.Control.geocoder({
        collapsed: false,
        showResultIcons: true,
        placeholder: description,
        geocoder: new L.Control.Geocoder.Nominatim({geocodingQueryParams: {countrycodes: 'cr'}})
      })
      .on('markgeocode', event => {
        if (angular.isFunction(callback)) {
          callback(event);
        }
      })
      .addTo(map);
    },
    addMarker(markerData, map) {
      const iconBlack = L.icon({
        iconUrl: '../../img/truck-pointer.png',
        iconSize: [30, 47], // size of the icon
        iconAnchor: [10, 40], // point of the icon which will correspond to marker's location
        popupAnchor: [-5, -100] // point from which the popup should open relative to the iconAnchor
      });
      const popup = L.popup({minWidth: 300})
      .setContent(markerData.innerHTML);
      const marker = L.marker([markerData.latitude, markerData.longitude], {icon: iconBlack}).addTo(map);
      marker.bindPopup(popup);
    }
  };
}

export default leafletService;
