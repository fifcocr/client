class LeafletController {

  /* @ngInject */
  constructor($scope, leafletService, $timeout, $compile) {
    const vm = this;
    vm.map = {};
    $timeout(drawMap);

    function drawMap() {
      vm.map = leafletService.drawMap(
        vm.name,
        {
          latitude: vm.latitude,
          longitude: vm.longitude
        },
        {
          actual: vm.actualZoom,
          minZoom: vm.minZoom,
          maxZoom: vm.maxZoom
        }
      );
      leafletService.addLayer(vm.map);
      leafletService.addZoomControl(vm.map);
      if (angular.isDefined(vm.origin)) {
        leafletService.addSearchControl('Origen', vm.map, data => {
          $scope.$apply(() => {
            vm.origin = data;
          });
        });
      }
      if (angular.isDefined(vm.destination)) {
        leafletService.addSearchControl('Destino', vm.map, data => {
          $scope.$apply(() => {
            vm.destination = data;
          });
        });
      }
      if (angular.isDefined(vm.markerList)) {
        vm.markerList().then(trips => {
          addMarkers(trips);
        });
      }
    }

    function addMarkers(trips) {
      const markers = _.filter(trips, trip => {
        return angular.isUndefined(trip.parentId);
      });
      _.forEach(markers, tripData => {
        if(tripData.status === 'accepted' || tripData.status === 'started') {
          const newScope = $scope.$new(true);
          newScope.data = tripData;
          leafletService.addMarker({
            latitude: tripData.destination.latitude,
            longitude: tripData.destination.longitude,
            innerHTML: $compile('<map-popup trip="data"></map-popup>')(newScope)[0]
          }, vm.map);
        }
      });
    }
  }
}

export default LeafletController;
