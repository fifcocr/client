import NavHeaderController from './nav-header.controller';
import './nav-header.less';

const navHeader = {
  template: require('./nav-header.html'),
  bindings: {
    admin: '@'
  },
  controller: NavHeaderController,
  controllerAs: 'vm'
};

export default
  angular
    .module('nav-header', [])
    .component('navHeader', navHeader)
    .name;
