class NavHeaderController {

  /* @ngInject */
  constructor($state, notificationsService) {
    const vm = this;
    vm.isActive = isActive;
    vm.getPendingTripsCount = getPendingTripsCount;
    vm.state = $state.current.name;

    function isActive(state) {
      return state === $state.current.name;
    }

    function getPendingTripsCount() {
      return notificationsService.getPendingTripsCount();
    }
  }
}

export default NavHeaderController;
