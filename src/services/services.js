import firebaseProvider from './firebase.provider';
import authService from './auth/auth.service';
import offererApplicationService from './offerer-application/offerer-application.service';
import carrierService from './carriers/carrier.service';
import tripService from './trips/trips.service';
import notificationsService from './notifications/notifications.service';
import pushNotificationsService from './push-notifications/push-notifications.service';

export default
  angular
  .module('fifco.services', [])
  .provider('firebase', firebaseProvider)
  .factory('authService', authService)
  .factory('offererApplicationService', offererApplicationService)
  .factory('carrierService', carrierService)
  .factory('tripService', tripService)
  .factory('notificationsService', notificationsService)
  .factory('pushNotificationsService', pushNotificationsService)
  .constant('RESOURCE', {
    IonicPushApiUrl: 'https://api.ionic.io/push/notifications',
    IonicPushApiAuthorizationToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI5ODA1NDk1My1iYjRhLTQ2N2ItOTVmMC01OGE5NTFiODQ1Y2MifQ.qCQERUmghI7Fisy1T9q5-xJiCQwHUkj0AVHfVNRZxK8'
  })
  .name;
