/* @ngInject */
function authService(firebase, $firebaseAuth) {
  const auth = $firebaseAuth(firebase.auth());
  return {
    loginWithEmail(email, password) {
      return auth.$signInWithEmailAndPassword(email, password);
    },
    registerWithEmail(email, password) {
      return auth.$createUserWithEmailAndPassword(email, password);
    }
  };
}

export default authService;

