/* @ngInject */
function applicantService(firebase, $firebaseArray, $firebaseObject) {
  const service = this;
  service.ref = firebase.database().ref('applications');
  service.carriers = $firebaseArray(service.ref);
  return {
    loadCarriers() {
      return service.carriers.$loaded();
    },
    loadCarrier(id) {
      const obj = $firebaseObject(service.ref.child(id));
      return obj.$loaded();
    }
  };
}

export default applicantService;

