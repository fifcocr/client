/* @ngInject */
function offererApplicationService(firebase, $firebaseArray) {
  const service = this;
  service.ref = firebase.database().ref('applications');
  service.applications = $firebaseArray(service.ref);
  return {
    saveApplication(application) {
      return service.applications.$add(application);
    }
  };
}

export default offererApplicationService;

