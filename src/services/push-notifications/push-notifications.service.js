/* @ngInject */
function pushNotificationsService(firebase, $http, $firebaseArray, RESOURCE) {
  const service = this;
  service.ref = firebase.database().ref().child('tokens');
  service.tokenList = $firebaseArray(service.ref);
  return {
    sendNotificationAllUsers(notification) {
      const postRequest = {
        method: 'POST',
        url: RESOURCE.IonicPushApiUrl,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${RESOURCE.IonicPushApiAuthorizationToken}`
        },
        data: notification
      };
      return $http(postRequest);
    },
    getTokens() {
      const tokenList = [];
      service.tokenList.$loaded()
        .then(devices => {
          _.forEach(devices, device => {
            tokenList.push(device.token);
          });
        });
      return tokenList;
    }
  };
}

export default pushNotificationsService;
