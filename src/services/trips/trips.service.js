/* @ngInject */
function tripService(firebase, $firebaseArray, $firebaseObject) {
  const service = this;
  service.ref = firebase.database().ref('trips');
  service.trips = $firebaseArray(service.ref);
  return {
    getTrips() {
      return service.trips.$loaded();
    },
    getTrip(id) {
      const obj = $firebaseObject(service.ref.child(id));
      return obj.$loaded();
    },
    saveTrip(trip) {
      return service.trips.$add(trip);
    }
  };
}

export default tripService;
