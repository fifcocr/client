/* @ngInject */
function notificationsService(firebase, $firebaseArray, $q) {
  const service = this;
  service.tripsRef = firebase.database().ref('trips');
  service.ref = service.tripsRef.orderByChild('offerted').equalTo(true);
  service.trips = $firebaseArray(service.ref);
  return {
    getPendingTrips() {
      return service.trips;
    },
    updateTrip(trip) {
      if (trip.parentId) {
        const checkpointsRef = service.tripsRef.child(trip.parentId).child('checkpoints');
        const checkpoints = $firebaseArray(checkpointsRef);
        return $q.all([
          checkpoints.$add(trip.origin),
          checkpoints.$add(trip.destination),
          service.trips.$save(trip)
        ]);
      }
      return service.trips.$save(trip);
    },
    getPendingTripsCount() {
      return _.filter(service.trips, trip => {
        return trip.status === 'pending' || (trip.status === 'finished' && !trip.parentId);
      }).length;
    }
  };
}

export default notificationsService;
