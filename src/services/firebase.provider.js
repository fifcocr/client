import firebase from 'firebase';

function firebaseProvider() {
  return {
    $get: () => {
      return firebase;
    }
  };
}

export default firebaseProvider;

