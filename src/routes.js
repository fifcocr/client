export {routesConfig, redirectHandler};

/* @ngInject */
function routesConfig($urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/');
}

/* @ngInject */
function redirectHandler($rootScope, $state) {
  const deregistration = $rootScope.$on('$stateChangeStart', (evt, to, params) => {
    if (to.redirectTo) {
      evt.preventDefault();
      $state.go(to.redirectTo, params, {location: 'replace'});
    }
  });

  $rootScope.$on('$destroy', deregistration);
}
