import angular from 'angular';
import firebase from 'firebase/app';
import angularfire from 'angularfire';
import 'lodash';

import pages from './pages/pages';
import services from './services/services';
import 'angular-ui-router';
import 'ng-notify';
import {routesConfig, redirectHandler} from './routes';
import './index.less';
import '../node_modules/leaflet-control-geocoder/dist/Control.Geocoder.js';

export default
    angular
      .module('fifco', ['ui.router', 'ngNotify', angularfire, pages, services])
      .config(routesConfig)
      .run(redirectHandler)
      .run(() => {
        firebase.initializeApp(FIREBASE_CONFIG);
      });
