module.exports = {
  extends: [
    'angular'
  ],
  rules: {
    'angular/no-service-method': 0
  },
  globals: {
    L: true,
    _: true
  }
}
