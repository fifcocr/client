import LoginController from './login.controller.js';
import configRoutes from './login.routes';
import './login.less';

export default
    angular
      .module('pages.login', [])
      .config(configRoutes)
      .controller('LoginController', LoginController)
      .name;

