class LoginController {

  /* @ngInject */
  constructor($state, $log, authService, ngNotify) {
    const vm = this;
    vm.loginUser = loginUser;

    function loginUser(email, password) {
      authService.loginWithEmail(email, password)
        .then(loginSuccess)
        .catch(loginFailure);
    }

    function loginSuccess() {
      $state.go('home');
    }

    function loginFailure(error) {
      $log.error(error);
      const wrongPasswordCode = 'auth/wrong-password';
      const noUserCode = 'auth/user-not-found';

      switch (error.code) {
        case wrongPasswordCode:
          ngNotify.set('Contrase\u00f1a equivocada.', 'error');
          break;

        case noUserCode:
          ngNotify.set('Usuario no existe.', 'error');
          break;

        default:
          ngNotify.set('Error. Intente de nuevo.', 'error');
          break;
      }
    }
  }
}

export default LoginController;

