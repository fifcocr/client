/* @ngInject */
function configRoutes($stateProvider) {
  $stateProvider
    .state('login', {
      url: '/',
      template: require('./login.html'),
      controller: 'LoginController as vm'
    });
}

export default configRoutes;
