import login from './login/login';
import register from './register/register';
import home from './home/home';
import newTrip from './new-trip/new-trip';
import joinTrip from './join-trip/join-trip';
import admin from './admin/admin';
import offererApplication from './offerer-application/offerer-application';
import notifications from './notifications/notifications';

export default
    angular
      .module('fifco.pages', [
        login,
        register,
        home,
        admin,
        newTrip,
        joinTrip,
        offererApplication,
        notifications
      ])
      .name;
