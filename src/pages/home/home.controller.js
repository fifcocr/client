class HomeController {

  /* @ngInject */
  constructor(tripService) {
    const vm = this;
    vm.markerList = tripService.getTrips;
  }
}

export default HomeController;
