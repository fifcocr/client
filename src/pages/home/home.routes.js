/* @ngInject */
function configRoutes($stateProvider) {
  $stateProvider
    .state('home', {
      url: '/home',
      template: require('./home.html'),
      controller: 'HomeController as vm'
    });
}

export default configRoutes;

