import navHeader from '../../common/components/nav-header/nav-header';
import configRoutes from './home.routes';
import HomeController from './home.controller';
import './home.less';
import leaflet from '../../common/components/leaflet/leaflet';

export default
    angular
      .module('pages.home', [
        navHeader,
        leaflet
      ])
      .controller('HomeController', HomeController)
      .config(configRoutes)
      .name;
