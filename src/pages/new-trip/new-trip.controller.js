class NewTripController {

  /* @ngInject */
  constructor(tripService, ngNotify, $state, pushNotificationsService, $log) {
    const vm = this;
    vm.generateTicket = generateTicket;
    vm.getPalletsToUse = getPalletsToUse;
    vm.origin = '';
    vm.destination = '';
    vm.trip = {};
    vm.tokenList = pushNotificationsService.getTokens();
    vm.trip.transport = {};
    vm.trip.transport.palletsToUse = 0;
    vm.originDatepicker = {
      formats: ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'],
      format: 'dd-MM-yyyy',
      altInputFormats: ['M!/d!/yyyy'],
      popup: {
        opened: false
      },
      options: {
        dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
      }
    };

    vm.destinationDatePicker = angular.copy(vm.originDatepicker);

    function disabled(data) {
      const date = data.date;
      const mode = data.mode;
      return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    function loadData() {
      const regex = /\,\ Provincia\ [a-zA-Z0-9 ,]+/g; // eslint-disable-line no-useless-escape
      vm.trip.status = 'pending';
      vm.trip.companyName = 'Gash'; // Only for demo
      vm.trip.transport.totalPallets = getPalletsToUse();
      vm.trip.ticketId = Math.round(Math.random() * (1000 - 1) + 1); // Only for demo
      vm.trip.origin.place = vm.origin.geocode.properties.display_name.replace(regex, "");
      vm.trip.origin.latitude = vm.origin.geocode.properties.lat;
      vm.trip.origin.longitude = vm.origin.geocode.properties.lon;
      vm.trip.origin.date = vm.trip.origin.date.getTime();
      vm.trip.destination.place = vm.destination.geocode.properties.display_name.replace(regex, "");
      vm.trip.destination.latitude = vm.destination.geocode.properties.lat;
      vm.trip.destination.longitude = vm.destination.geocode.properties.lon;
      vm.trip.destination.date = vm.trip.destination.date.getTime();
    }

    function generateTicket() {
      loadData();
      tripService
        .saveTrip(vm.trip)
        .then(submissionSuccess)
        .then(sendNotification)
        .then(() => {
          vm.trip = {};
        })
        .then(redirectToHome)
        .catch(submissionError);
    }

    function submissionSuccess() {
      ngNotify.set('Sus datos han sido enviados.', 'success');
    }

    function submissionError() {
      ngNotify.set('Lo sentimos algo ha salido mal.', 'error');
    }

    function redirectToHome() {
      $state.go('home');
    }

    function sendNotification() {
      const notification = {};
      notification.tokens = vm.tokenList;
      notification.profile = 'dev';
      notification.notification = {
        title: 'Nueva solicitud',
        message: 'Se ha creado una nueva solicitud'
      };
      pushNotificationsService.sendNotificationAllUsers(notification)
        .then(() => {
          $log.info('Notification Success');
        })
        .catch(error => {
          $log.info('Notification Error', error);
        });
    }

    function getPalletsToUse() {
      switch (vm.trip.transport.dimension) {
        case '1 ton':
          return 1;
        case '1.5 ton':
          return 2;
        case '3.5 ton':
          return 6;
        case '4 ton':
          return 7;
        case '7 ton':
          return 12;
        default:
          return 0;
      }
    }
  }
}

export default NewTripController;
