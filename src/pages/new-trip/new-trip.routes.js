/* @ngInject */
function configRoutes($stateProvider) {
  $stateProvider
    .state('newTrip', {
      url: '/trips/new',
      template: require('./new-trip.html'),
      controller: 'NewTripController as vm',
      redirectTo: 'newTrip.step1'
    })
    .state('newTrip.step1', {
      url: '',
      template: require('./step1.html')
    })
    .state('newTrip.step2', {
      url: '',
      template: require('./step2.html')
    })
    .state('newTrip.step3', {
      url: '',
      template: require('./step3.html')
    })
    .state('newTrip.step4', {
      url: '',
      template: require('./step4.html')
    })
    .state('newTrip.step5', {
      url: '',
      template: require('./step5.html')
    });
}

export default configRoutes;
