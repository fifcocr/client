import configRoutes from './new-trip.routes';
import NewTripController from './new-trip.controller';
import stepIndicator from '../../common/components/step-indicator/step-indicator';
import imageOption from '../../common/components/image-option/image-option';
import './new-trip.less';
import leaflet from '../../common/components/leaflet/leaflet';
import datepickerPopup from 'angular-ui-bootstrap/src/datepickerPopup';

export default
  angular
    .module('pages.new-trip', [
      stepIndicator,
      imageOption,
      leaflet,
      datepickerPopup
    ])
    .controller('NewTripController', NewTripController)
    .config(configRoutes)
    .name;

