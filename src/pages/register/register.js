import RegisterController from './register.controller';
import configRoutes from './register.routes';
import './register.less';

export default
  angular
    .module('pages.register', [])
    .controller('RegisterController', RegisterController)
    .config(configRoutes)
    .name;

