/* @ngInject */
function configRoutes($stateProvider) {
  $stateProvider
    .state('register', {
      url: '/register',
      template: require('./register.html'),
      controller: 'RegisterController as vm'
    });
}

export default configRoutes;

