class RegisterController {

  /* @ngInject */
  constructor($state, $log, authService) {
    const vm = this;
    vm.registerUser = registerUser;

    function registerUser(email, password) {
      authService.registerWithEmail(email, password)
        .then(registerSuccess)
        .catch(registerFailure);
    }

    function registerSuccess() {
      // registrar otros datos en BD con response.uid
      $state.go('home');
    }

    function registerFailure(error) {
      $log.error(error);
    }
  }
}

export default RegisterController;

