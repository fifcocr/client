class NotificationsController {

  /* @ngInject */
  constructor(notificationsService, pushNotificationsService, $log) {
    const vm = this;
    vm.acceptTrip = acceptTrip;
    vm.rejectTrip = rejectTrip;
    vm.selectStar = selectStar;
    vm.offerList = notificationsService.getPendingTrips();
    vm.tokenList = pushNotificationsService.getTokens();

    function acceptTrip(trip) {
      trip.status = 'accepted';
      notificationsService
        .updateTrip(trip)
        .then(sendAcceptedNotification);
    }

    function rejectTrip(trip) {
      trip.status = 'pending';
      delete trip.offerted;
      delete trip.offer;
      notificationsService
        .updateTrip(trip)
        .then(sendRejectedNotification);
    }

    function selectStar(star) {
      vm.selectedStar = star;
    }

    function sendAcceptedNotification() {
      const notification = {};
      notification.tokens = vm.tokenList;
      notification.profile = 'dev';
      notification.notification = {
        title: 'Viaje aceptado',
        message: 'Se ha aceptado un nuevo viaje'
      };
      pushNotificationsService.sendNotificationAllUsers(notification)
        .then(() => {
          $log.info('Notification Success');
        })
        .catch(error => {
          $log.error('Notification Error', error);
        });
    }

    function sendRejectedNotification() {
      const notification = {};
      notification.tokens = vm.tokenList;
      notification.profile = 'dev';
      notification.notification = {
        title: 'Oferta rechazada',
        message: 'El presupuesto del viaje ha sido rechazado.'
      };
      pushNotificationsService.sendNotificationAllUsers(notification)
        .then(() => {
          $log.info('Notification Success');
        })
        .catch(error => {
          $log.error('Notification Error', error);
        });
    }
  }
}

export default NotificationsController;
