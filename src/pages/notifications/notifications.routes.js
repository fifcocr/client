/* @ngInject */
function configRoutes($stateProvider) {
  $stateProvider
    .state('notifications', {
      url: '/notifications',
      template: require('./notifications.html'),
      controller: 'NotificactionsController as vm'
    });
}

export default configRoutes;
