import NotificactionsController from './notifications.controller.js';
import configRoutes from './notifications.routes';
import './notifications.less';
import popover from 'angular-ui-bootstrap/src/popover';

export default
  angular
    .module('pages.notifications', [
      popover
    ])
    .config(configRoutes)
    .controller('NotificactionsController', NotificactionsController)
    .name;

