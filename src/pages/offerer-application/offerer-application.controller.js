class OffererApplicationController {

  /* @ngInject */
  constructor(firebase, offererApplicationService, ngNotify, OPTIONS) {
    const vm = this;
    vm.options = OPTIONS;
    vm.hide = false;

    vm.submitApplication = submitApplication;

    vm.next = next;

    function next() {
      vm.hide = !vm.hide;
    }

    function submitApplication(form) {
      form.model.status = 'APPLICANT';
      offererApplicationService
        .saveApplication(form.model)
        .then(submissionSuccess)
        .then(() => {
          form.model = {};
          form.$setPristine();
          form.$setUntouched();
        })
        .catch(submissionError);
    }

    function submissionSuccess() {
      ngNotify.set('Sus datos han sido enviados.', 'success');
    }

    function submissionError() {
      ngNotify.set('Lo sentimos algo ha salido mal.', 'error');
    }
  }
}

export default OffererApplicationController;

