import OffererApplicationController from './offerer-application.controller.js';
import configRoutes from './offerer-application.routes';
import './offerer-application.less';

const OPTIONS = {
  services: ['Con chofer', 'Sin chofer', 'Chambero'],
  equipment: ['Transporte pesado', 'Refrigerado', 'Seco', 'Rampa', 'Especializado'],
  coverageOptions: ['Todo el país', 'San José', 'Cartago', 'Heredia', 'Alajuela', 'Limón', 'Puntarenas', 'Guanacaste', 'GAM'],
  availabilityOptions: ['24/7', 'Diurna', 'Nocturna']
};

export default
  angular
    .module('pages.offerer-application', [])
    .config(configRoutes)
    .constant('OPTIONS', OPTIONS)
    .controller('OffererApplicationController', OffererApplicationController)
    .name;

