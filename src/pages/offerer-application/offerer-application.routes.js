/* @ngInject */
function configRoutes($stateProvider) {
  $stateProvider
    .state('offererApplication', {
      url: '/apply',
      template: require('./offerer-application.html'),
      controller: 'OffererApplicationController as vm'
    });
}

export default configRoutes;
