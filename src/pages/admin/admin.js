import navHeader from '../../common/components/nav-header/nav-header';
import configRoutes from './admin.routes';
import offerer from './offerers/offerers';
import applicant from './applicants/applicants';
import preSelected from './pre-selected/pre-selected';
import './admin.less';

export default
    angular
      .module('pages.admin', [
        navHeader,
        offerer,
        preSelected,
        applicant

      ])
      .config(configRoutes)
      .name;
