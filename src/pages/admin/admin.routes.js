/* @ngInject */
function configRoutes($stateProvider) {
  $stateProvider
    .state('admin', {
      url: '/admin',
      template: require('./admin.html'),
      redirectTo: 'admin.offerers'
    });
}

export default configRoutes;
