/* @ngInject */
function configRoutes($stateProvider) {
  $stateProvider
    .state('admin.applicants', {
      url: '/applicants',
      template: require('./list/view.html'),
      controller: 'ApplicantListController as vm'
    })
    .state('admin.applicant', {
      url: '/applicants/{id}',
      template: require('./details/view.html'),
      controller: 'ApplicantDetailsController as vm'
    });
}

export default configRoutes;
