class ApplicantListController {
  /* @ngInject */
  constructor(carrierService) {
    carrierService
      .loadCarriers()
      .then(carriers => {
        this.carriers = carriers;
      });
  }

  applicantFilter(carrier) {
    return carrier.status === 'APPLICANT';
  }
}

export default ApplicantListController;
