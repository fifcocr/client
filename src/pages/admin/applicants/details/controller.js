class ApplicantDetailsController {
  /* @ngInject */
  constructor(carrierService, $state, $stateParams) {
    this.$state = $state;
    carrierService
      .loadCarrier($stateParams.id)
      .then(carrier => {
        this.carrier = carrier;
      });
  }

  discardCarrier() {
    this.carrier.status = 'DISCARDED';
    this.carrier.$save().then(() => {
      this.$state.go('admin.applicants');
    });
  }

  preselectCarrier() {
    this.carrier.status = 'PRE-SELECTED';
    this.carrier.$save().then(() => {
      this.$state.go('admin.pre-selected');
    });
  }

  selectCarrier() {
    this.carrier.status = 'OFFERER';
    this.carrier.$save().then(() => {
      this.$state.go('admin.offerers');
    });
  }
}

export default ApplicantDetailsController;
