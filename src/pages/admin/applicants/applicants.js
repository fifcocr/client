import configRoutes from './applicants.routes';
import ApplicantListController from './list/controller';
import ApplicantDetailsController from './details/controller';

export default
    angular
      .module('pages.applicants', [])
      .controller('ApplicantListController', ApplicantListController)
      .controller('ApplicantDetailsController', ApplicantDetailsController)
      .config(configRoutes)
      .name;
