class PreSelectedController {
  /* @ngInject */
  constructor(carrierService) {
    carrierService
      .loadCarriers()
      .then(carriers => {
        this.carriers = carriers;
      });
  }

  preselectedFilter(carrier) {
    return carrier.status === 'PRE-SELECTED';
  }
}

export default PreSelectedController;
