/* @ngInject */
function configRoutes($stateProvider) {
  $stateProvider
    .state('admin.pre-selected', {
      url: '/pre-selected',
      template: require('./list/view.html'),
      controller: 'PreSelectedListController as vm'
    })
    .state('admin.pre-selected-details', {
      url: '/pre-selected/{id}',
      template: require('./details/view.html'),
      controller: 'PreSelectedDetailsController as vm'
    });
}

export default configRoutes;
