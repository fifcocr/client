class PreSelectedDetailsController {
  /* @ngInject */
  constructor(carrierService, $state, $stateParams) {
    this.$state = $state;
    carrierService
      .loadCarrier($stateParams.id)
      .then(carrier => {
        this.carrier = carrier;
      });
  }

  discardCarrier() {
    this.carrier.status = 'DISCARDED';
    this.carrier.$save().then(() => {
      this.$state.go('admin.applicants');
    });
  }

  selectCarrier() {
    this.carrier.status = 'OFFERER';
    this.carrier.$save().then(() => {
      this.$state.go('admin.offerers');
    });
  }
}

export default PreSelectedDetailsController;
