import configRoutes from './pre-selected.routes';
import PreSelectedListController from './list/controller';
import PreSelectedDetailsController from './details/controller';

export default
    angular
      .module('pages.pre-selected', [])
      .controller('PreSelectedListController', PreSelectedListController)
      .controller('PreSelectedDetailsController', PreSelectedDetailsController)
      .config(configRoutes)
      .name;
