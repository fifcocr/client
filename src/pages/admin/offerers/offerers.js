import configRoutes from './offerers.routes';
import OffererListController from './list/controller';
import OffererDetailsController from './details/controller';

export default
    angular
      .module('pages.offerers', [])
      .controller('OffererListController', OffererListController)
      .controller('OffererDetailsController', OffererDetailsController)
      .config(configRoutes)
      .name;
