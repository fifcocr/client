/* @ngInject */
function configRoutes($stateProvider) {
  $stateProvider
    .state('admin.offerers', {
      url: '/offerers',
      template: require('./list/view.html'),
      controller: 'OffererListController as vm'
    })
    .state('admin.offerer', {
      url: '/offerers/{id}',
      template: require('./details/view.html'),
      controller: 'OffererDetailsController as vm'
    });
}

export default configRoutes;
