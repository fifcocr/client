class OfferersController {
  /* @ngInject */
  constructor(carrierService) {
    carrierService
      .loadCarriers()
      .then(carriers => {
        this.carriers = carriers;
      });
  }

  offerersFilter(carrier) {
    return carrier.status === 'OFFERER';
  }
}

export default OfferersController;
