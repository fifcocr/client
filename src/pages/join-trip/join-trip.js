import configRoutes from './join-trip.routes';
import JoinTripController from './join-trip.controller';
import imageOption from '../../common/components/image-option/image-option';
import leaflet from '../../common/components/leaflet/leaflet';
import './join-trip.less';
import datepickerPopup from 'angular-ui-bootstrap/src/datepickerPopup';

export default
  angular
    .module('pages.join-trip', [
      leaflet,
      imageOption,
      datepickerPopup
    ])
    .controller('JoinTripController', JoinTripController)
    .config(configRoutes)
    .name;

