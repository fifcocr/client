/* @ngInject */
function configRoutes($stateProvider) {
  $stateProvider
    .state('joinTrip', {
      url: '/trips/join/:id',
      template: require('./join-trip.html'),
      controller: 'JoinTripController as vm'
    });
}

export default configRoutes;
