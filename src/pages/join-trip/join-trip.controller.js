class JoinTripController {

  /* @ngInject */
  constructor(tripService, $stateParams, $state, pushNotificationsService, ngNotify, $log) {
    const vm = this;
    vm.origin = '';
    vm.destination = '';
    vm.generateTicket = generateTicket;
    vm.getRemainingPallets = getRemainingPallets;
    vm.tokenList = pushNotificationsService.getTokens();
    vm.trip = {transport: {}};
    tripService
      .getTrip($stateParams.id)
      .then(trip => {
        vm.trip = trip;
        const copy = angular.copy(trip);
        copy.origin = null;
        copy.offer = null;
        copy.destination = null;
        copy.freight.description = null;
        copy.transport.palletsToUse = 0;
        copy.$id = null;
        copy.ticketId = Math.round(Math.random() * (1000 - 1) + 1);

        vm.model = copy;
      });

    function loadData() {
      const regex = /\,\ Provincia\ [a-zA-Z0-9 ,]+/g; // eslint-disable-line no-useless-escape
      vm.model.status = 'pending';
      vm.model.origin.place = vm.origin.geocode.properties.display_name.replace(regex, "");
      vm.model.origin.latitude = vm.origin.geocode.properties.lat;
      vm.model.origin.longitude = vm.origin.geocode.properties.lon;
      vm.model.destination.place = vm.destination.geocode.properties.display_name.replace(regex, "");
      vm.model.destination.latitude = vm.destination.geocode.properties.lat;
      vm.model.destination.longitude = vm.destination.geocode.properties.lon;
      vm.model.parentId = vm.trip.$id;
    }

    function getRemainingPallets() {
      return vm.trip.transport.totalPallets - vm.trip.transport.palletsToUse;
    }

    function generateTicket() {
      loadData();
      vm.model.offerted = false;
      tripService
        .saveTrip(vm.model)
        .then(submissionSuccess)
        .then(sendNotification)
        .then(() => {
          vm.model = {};
        })
        .then(redirectToHome)
        .catch(submissionError);
    }

    function submissionSuccess() {
      ngNotify.set('Sus datos han sido enviados.', 'success');
    }

    function submissionError() {
      ngNotify.set('Lo sentimos algo ha salido mal.', 'error');
    }

    function redirectToHome() {
      $state.go('home');
    }

    function sendNotification() {
      const notification = {};
      notification.tokens = vm.tokenList;
      notification.profile = 'dev';
      notification.notification = {
        title: 'Nueva solicitud para unirse a uno de tus viajes',
        message: `Se ha creado una nueva solicitud para unirse al viaje ${vm.trip.ticketId}`
      };
      pushNotificationsService.sendNotificationAllUsers(notification)
        .then(() => {
          $log.info('Notification Success');
        })
        .catch(error => {
          $log.info('Notification Error', error);
        });
    }
  }
}

export default JoinTripController;
